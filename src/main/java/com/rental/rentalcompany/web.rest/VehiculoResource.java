package com.rental.rentalcompany.web.rest;

import com.rental.rentalcompany.service.IVehiculoService;
import com.rental.rentalcompany.service.dto.VehiculoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Optional;

@RestController
@RequestMapping("/api/vehiculos")
@CrossOrigin("http://localhost:4200")
public class VehiculoResource {
    @Autowired
    private IVehiculoService vehiculoService;

    @PostMapping
    public ResponseEntity create(@RequestBody VehiculoDTO vehiculoDTO){
        return vehiculoService.create(vehiculoDTO);
    }

    @GetMapping
    public Page<VehiculoDTO> read(@PathParam("pageSize")Integer pageSize,
                                  @PathParam("pageNumber")Integer pageNumber){
        return  vehiculoService.read(pageSize, pageNumber);
    }
}
