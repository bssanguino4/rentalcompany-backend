package com.rental.rentalcompany.web.rest;

import com.rental.rentalcompany.service.IClienteService;
import com.rental.rentalcompany.service.dto.ClienteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api/clients")
@CrossOrigin("http://localhost:4200")
public class ClienteResource {
    @Autowired
    private IClienteService clienteService;

    @PostMapping
    public ResponseEntity create(@RequestBody ClienteDTO clienteDTO){
        return clienteService.create(clienteDTO);
    }
    @GetMapping
    public Page<ClienteDTO> read(@PathParam("pageSize")Integer pageSize,
                                 @PathParam("pageNumber")Integer pageNumber){
        return clienteService.read(pageSize, pageNumber);
    }
}
