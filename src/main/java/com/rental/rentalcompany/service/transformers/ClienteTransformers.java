package com.rental.rentalcompany.service.transformers;

import com.rental.rentalcompany.domain.Cliente;
import com.rental.rentalcompany.service.dto.ClienteDTO;

public class ClienteTransformers {

    public static ClienteDTO getClienteDTOFromCliente(Cliente cliente){
        if (cliente == null){
            return null;
        }
        ClienteDTO dto = new ClienteDTO();
        dto.setId(cliente.getId());
        dto.setNombre(cliente.getNombre());
        dto.setTelefono(cliente.getTelefono());
        dto.setDireccion(cliente.getDireccion());
        dto.setEmail(cliente.getEmail());
        dto.setNumeroDocumento(cliente.getNumeroDocumento());
        return dto;
    }

    public static Cliente getClienteFromClienteDTO(ClienteDTO clienteDTO){
        if (clienteDTO == null){
            return null;
        }
        Cliente cliente = new Cliente();
        cliente.setId(clienteDTO.getId());
        cliente.setNombre(clienteDTO.getNombre());
        cliente.setTelefono(clienteDTO.getTelefono());
        cliente.setDireccion(clienteDTO.getDireccion());
        cliente.setEmail(clienteDTO.getEmail());
        cliente.setNumeroDocumento(clienteDTO.getNumeroDocumento());
        return cliente;
    }
}
