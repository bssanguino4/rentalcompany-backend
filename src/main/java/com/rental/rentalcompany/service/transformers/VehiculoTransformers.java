package com.rental.rentalcompany.service.transformers;

import com.rental.rentalcompany.domain.Vehiculo;
import com.rental.rentalcompany.service.dto.VehiculoDTO;

public class VehiculoTransformers {
    public static VehiculoDTO getVehiculoDTOFromVehiculo(Vehiculo vehiculo){
        if (vehiculo == null){
            return null;
        }
        VehiculoDTO dto = new VehiculoDTO();
        dto.setId(vehiculo.getId());
        dto.setFechaAlquiler(vehiculo.getFechaAlquiler());
        dto.setFechaDevolucion(vehiculo.getFechaDevolucion());
        dto.setNumeroPlaca(vehiculo.getNumeroPlaca());
        dto.setValorAlquiler(vehiculo.getValorAlquiler());
        dto.setCliente(vehiculo.getCliente());
        return dto;
    }

    public static Vehiculo getVehiculoFromVehiculoDTO(VehiculoDTO vehiculoDTO){
        if (vehiculoDTO == null){
            return null;
        }
        Vehiculo vehiculo = new Vehiculo();
        vehiculo.setId(vehiculoDTO.getId());
        vehiculo.setFechaAlquiler(vehiculoDTO.getFechaAlquiler());
        vehiculo.setFechaDevolucion(vehiculoDTO.getFechaDevolucion());
        vehiculo.setNumeroPlaca(vehiculoDTO.getNumeroPlaca());
        vehiculo.setValorAlquiler(vehiculoDTO.getValorAlquiler());
        vehiculo.setCliente(vehiculoDTO.getCliente());
        return vehiculo;
    }
}
