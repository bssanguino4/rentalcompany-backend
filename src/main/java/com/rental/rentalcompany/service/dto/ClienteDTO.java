package com.rental.rentalcompany.service.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class ClienteDTO implements Serializable {
    @NotNull

    private Long id;

    private String nombre;

    private String telefono;

    private String direccion;

    private String email;

    private String numeroDocumento;
}
