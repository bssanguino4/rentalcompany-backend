package com.rental.rentalcompany.service.dto;

import com.rental.rentalcompany.domain.Cliente;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class VehiculoDTO implements Serializable {
    @NotNull

    private int id;

    private Date fechaAlquiler;

    private Date fechaDevolucion;

    private String numeroPlaca;

    private String valorAlquiler;

    private Cliente cliente;
}
