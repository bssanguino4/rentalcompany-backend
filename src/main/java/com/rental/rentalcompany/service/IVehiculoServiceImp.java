package com.rental.rentalcompany.service;

import com.rental.rentalcompany.domain.Vehiculo;
import com.rental.rentalcompany.repository.VehiculoRepository;
import com.rental.rentalcompany.service.dto.VehiculoDTO;
import com.rental.rentalcompany.service.transformers.VehiculoTransformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class IVehiculoServiceImp implements IVehiculoService{
    @Autowired
    private VehiculoRepository vehiculoRepository;

    @Override
    public ResponseEntity create(VehiculoDTO vehiculoDTO) {
        Vehiculo vehiculo = VehiculoTransformers.getVehiculoFromVehiculoDTO(vehiculoDTO);
        if(vehiculoRepository.findById(vehiculo.getId()).isPresent()){
            return new ResponseEntity("El ID ya se encuentra en uso", HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity(VehiculoTransformers.getVehiculoDTOFromVehiculo(vehiculoRepository.save(vehiculo)),HttpStatus.OK);
        }
    }

    @Override
    public Page<VehiculoDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return vehiculoRepository.findAll(pageable)
                .map(VehiculoTransformers::getVehiculoDTOFromVehiculo);
    }
}
