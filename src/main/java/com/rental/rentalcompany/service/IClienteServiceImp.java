package com.rental.rentalcompany.service;

import com.rental.rentalcompany.domain.Cliente;
import com.rental.rentalcompany.repository.ClienteRepository;
import com.rental.rentalcompany.service.dto.ClienteDTO;
import com.rental.rentalcompany.service.transformers.ClienteTransformers;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IClienteServiceImp implements IClienteService{
    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public ResponseEntity create(ClienteDTO clienteDTO) {
        Cliente cliente = ClienteTransformers.getClienteFromClienteDTO(clienteDTO);
        if (clienteRepository.findById(cliente.getId()).isPresent()){
            return new ResponseEntity("El Id Ya se encuentra en Uso", HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity(ClienteTransformers.getClienteDTOFromCliente(clienteRepository.save(cliente)), HttpStatus.OK);
        }
    }

    @Override
    public Page<ClienteDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return  clienteRepository.findAll(pageable)
                .map(ClienteTransformers::getClienteDTOFromCliente);
    }

}
