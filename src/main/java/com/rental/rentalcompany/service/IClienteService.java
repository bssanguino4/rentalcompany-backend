package com.rental.rentalcompany.service;

import com.rental.rentalcompany.service.dto.ClienteDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IClienteService {
    public ResponseEntity create(ClienteDTO clienteDTO);

    public Page<ClienteDTO> read(Integer pageSize, Integer pageNumber);


}
