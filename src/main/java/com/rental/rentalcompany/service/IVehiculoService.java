package com.rental.rentalcompany.service;

import com.rental.rentalcompany.domain.Vehiculo;
import com.rental.rentalcompany.service.dto.VehiculoDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface IVehiculoService {
    public ResponseEntity create(VehiculoDTO vehiculoDTO);

    public Page<VehiculoDTO> read(Integer pageSize, Integer pageNumber);
}
