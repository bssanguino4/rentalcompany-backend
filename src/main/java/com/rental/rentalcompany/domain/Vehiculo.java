package com.rental.rentalcompany.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Vehiculo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaAlquiler;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fechaDevolucion;

    private String numeroPlaca;

    private String valorAlquiler;

    @ManyToOne
    private Cliente cliente;
}
